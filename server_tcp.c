/*
Como compilar:
gcc server_tcp.c -o server -lpthread

Como usar:
./server
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "server_tcp.h"

#define PORT 4000

pthread_mutex_t mutex_sala = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_cliente = PTHREAD_MUTEX_INITIALIZER;

int sockfd;
struct sockaddr_in serv_addr;

cliente *users;
sala *salas;

char buffer[256];



sala *add_nova_sala(char* novo_nome, int novo_max_usuarios)
{
    sala* nova_sala = malloc(sizeof(sala));
    nova_sala->nome = malloc(sizeof(char)*strlen(novo_nome));
    strcpy(nova_sala->nome, novo_nome);
    nova_sala->max_usuarios = novo_max_usuarios;
    nova_sala->qte_usuarios = 0;

    //aloca a quantidade de listas de clientes
    nova_sala->clientes = malloc(sizeof(cliente *)*nova_sala->max_usuarios);
    int i;
    //seta todos os clientes para null
    for(i = 0; i < nova_sala->max_usuarios; i++){
			nova_sala->clientes[i] = NULL;
		}

    pthread_mutex_lock(&mutex_sala);
    nova_sala->prox = NULL;
  	if(salas == NULL){
  		salas = nova_sala;
  	}else{
  		sala *it;
  		it = salas;
  		while (it->prox != NULL){
  			it = it->prox;
  		}
  		it->prox = nova_sala;

  	}
    pthread_mutex_unlock(&mutex_sala);
    return nova_sala;
}

void entra_sala(char *nome, cliente *user){
	sala *escolhida = malloc(sizeof(sala));

	pthread_mutex_lock(&mutex_sala);
  //entrou numa sala tem que sair da outra
  if(user->sala != NULL)
    sai_sala(user);
	escolhida = acha_sala(nome);
	if (escolhida != NULL){
		//ainda da pra entrar
		if(escolhida->qte_usuarios < escolhida->max_usuarios){
			int i;
			for (i = 0; i < escolhida->max_usuarios; i++)
			{
				//acha final da lista de clientes
				if (escolhida->clientes[i] == NULL)
				{
					escolhida->clientes[i] = user;
					user->sala = escolhida;
					escolhida->qte_usuarios++;
					break;
				}
			}
		}else{
			bzero(buffer, 256);
			sprintf(buffer, "OPS! ESSA SALA JA ESTA CHEIA!");
			int n = write(user->socket, buffer, sizeof(buffer));
			if (n < 0)
			{
				printf("There was an Error writing to socket.");
			}
			return;
		}
	}else{
		//nao achou a sala
		bzero(buffer, 256);
		sprintf(buffer, "OPS! ESSA SALA NAO EXISTE!");
		int n = write(user->socket, buffer, sizeof(buffer));
		if (n < 0)
		{
			printf("There was an Error writing to socket.");
		}
		return;
	}
  pthread_mutex_unlock(&mutex_sala);

}

sala *acha_sala(char* nome){
	sala *iterador;

	for(iterador = salas; iterador !=NULL; iterador = iterador->prox){
		if(strcmp(nome, iterador->nome) == 0){
			return iterador;
		}
	}
	return NULL;
}

cliente *acha_cliente(int socket, sala *sala){
	int i;
	for (i =0; i<sala->qte_usuarios; i++){
		if(sala->clientes[i]->socket == socket){
			return sala->clientes[i];
		}
	}
	return NULL;
}

void sai_sala(cliente *user){
	sala *sala = malloc(sizeof(sala));
	int i;
	//pthread_mutex_lock(&mutex_sala);
  sala = acha_sala(user->sala->nome);
	for (i =0; i<sala->qte_usuarios; i++){
		if(strcmp(sala->clientes[i]->apelido, user->apelido)==0)
    {
			sala->clientes[i] == NULL;
			sala->qte_usuarios--;
		}
	}
  //pthread_mutex_unlock(&mutex_sala);
}

void manda_msg_todos(char *msg){
  pthread_mutex_lock(&mutex_cliente);
    char buffer[256];
    cliente *it;
    it = users;
    while(it != NULL)
    {
        snprintf(buffer, 256, "Uma nova sala chamada %s foi criada", msg);
        write(it->socket, buffer, sizeof(buffer));
        it = it->prox;
    }
  pthread_mutex_unlock(&mutex_cliente);
}

void manda_msg_grupo(cliente *user, char *msg){
	sala *sala = user->sala;
  int j;
  for(j=0;j<sala->qte_usuarios;j++)
	   puts(sala->clientes[j]->apelido);
  char buffer[256];

	int i;

	pthread_mutex_lock(&mutex_cliente);
	{
		for (i = 0; i < sala->qte_usuarios; i++)
		{
			if (sala->clientes[i] != NULL && sala->clientes[i] != user)
			{
				snprintf(buffer, 256, "[%s@%s]: %s",user->apelido, sala->nome, msg);
				write(sala->clientes[i]->socket, buffer, sizeof(buffer));
			}
		}
	}
	pthread_mutex_unlock(&mutex_cliente);
}

void lista_participantes(sala *sala, cliente *user){
  char buffer[256];

  int i;
  bzero(buffer,256);
  snprintf(buffer, 256, "PARTICIPANTES NA SALA:");
  write(user->socket, buffer, sizeof(buffer));
  //pthread_mutex_lock(&mutex_cliente);
  for (i=0;i<sala->qte_usuarios;i++){
    if(sala->clientes[i]!= NULL){
      bzero(buffer,256);
      snprintf(buffer, 256, "%s",sala->clientes[i]->apelido);
      write(user->socket, buffer, sizeof(buffer));
    }
  }
  //pthread_mutex_unlock(&mutex_cliente);
}

int comeca_com(const char *prefixo, const char *str)
{
	int comeca;

	comeca = strncmp(prefixo, str, strlen(prefixo));
	return comeca == 0;
}

void sair(cliente *user)
{
	sai_sala(user);
	pthread_mutex_lock(&mutex_cliente);
	{
		if (users == user)
		{
			users = user->prox;
		}
		else
		{
			cliente *it;
			for (it = users; it->prox != NULL; it->prox)
			{
				if (it->prox == user)
				{
					it->prox = it->prox->prox;
					break;
				}
			}
		}
		close(user->socket);
	}
	pthread_mutex_unlock(&mutex_cliente);
	pthread_exit(0);
}

void lista_salas(cliente *user){

  sala *it;
  it = salas;
  char buffer[256];

  bzero(buffer,256);
  snprintf(buffer, 256, "SALAS EXISTENTES");
  write(user->socket, buffer, sizeof(buffer));
  while(it != NULL){
    bzero(buffer,256);
    snprintf(buffer, 256, "%s (%d)",it->nome, it->qte_usuarios);
    write(user->socket, buffer, sizeof(buffer));
    it = it->prox;
  }
}
void ajuda(cliente *user){
  bzero(buffer, 256);
  sprintf(buffer, "Os comandos que voce pode executar sao:");
  int n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }
  bzero(buffer, 256);
  sprintf(buffer, "/create <nome_da_sala> - para criar uma nova sala");
  n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }
  bzero(buffer, 256);
  sprintf(buffer, "/join <nome_da_sala> - para entrar em uma sala existente");
  n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }
  bzero(buffer, 256);
  sprintf(buffer, "/leave - sair da sala em que esta e voltar para a Sala Geral");
  n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }
  bzero(buffer, 256);
  sprintf(buffer, "/list_rooms - para ver as salas existentes");
  n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }
  bzero(buffer, 256);
  sprintf(buffer, "/list_users - ver os participantes da sala atual");
  n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }

  bzero(buffer, 256);
  sprintf(buffer, "/nick <novo_apelido> - para alterar seu apelido");
  n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }
  bzero(buffer, 256);
  sprintf(buffer, "/exit - para sair do chat");
  n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }
  bzero(buffer, 256);
  sprintf(buffer, "/help - para exibir essa lista novamente");
  n = write(user->socket, buffer, sizeof(buffer));
  if (n < 0)
  {
    puts("There was an Error writing to socket.");
  }
}


void *client(void *arg){

	cliente *user = (cliente*)arg;


	while(1){
		bzero(buffer, 256);
		int n = read(user->socket, buffer, 256);

		if (n < 0)
		{
			puts("There was an Error reading from socket.\n");
			exit(-1);
		}
		puts(buffer);
		//ve se tem um comando no inicio
		if (comeca_com("/list_rooms", buffer)){
			lista_salas(user);
		}else if(comeca_com("/list_users", buffer)){
      lista_participantes(user->sala, user);
    }
		else if (comeca_com("/leave", buffer)){
				entra_sala("Sala Geral", user);
		}
		else if (comeca_com("/exit", buffer)){
			sair(user);
		}else if (comeca_com("/create", buffer)){
			  char *nome = strchr(buffer, ' ') + 1;
				sala *nova_sala = add_nova_sala(nome, 20);
      	manda_msg_todos(nova_sala->nome);
		}else if (comeca_com("/join", buffer)){
			char *name = strchr(buffer, ' ') + 1;
				entra_sala(name,user);
        sala *sala = acha_sala(name);
        lista_participantes(sala, user);
		}else if (comeca_com("/nick", buffer)){
			char *name = strchr(buffer, ' ') + 1;
				user->apelido = malloc(strlen(name) + 1);
				strcpy(user->apelido, name);
        bzero(buffer,256);
        snprintf(buffer, 256, "Voce mudou seu apelido para: %s",user->apelido);
        write(user->socket, buffer, sizeof(buffer));
		}else if(comeca_com("/help", buffer)){
      ajuda(user);
    }else{
      manda_msg_grupo(user, buffer);
    }
	}
}

int main(int argc, char *argv[])
{
	int sockfd, newsockfd, n;
	socklen_t clilen;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;

	users = NULL;
	salas = NULL;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        puts("ERROR opening socket");

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(serv_addr.sin_zero), 8);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		puts("ERROR on binding");

  add_nova_sala("Sala Geral", 20);

	listen(sockfd, 5);


	/* read from the socket */
	while(1){
		bzero(buffer, 256);

		//inicializando as estruturas do cliente se conectando
		cliente *current_user = malloc(sizeof(cliente));
		current_user->thr = malloc(sizeof(pthread_t));
		current_user->addr = malloc(sizeof(struct sockaddr_in));
		current_user->socket_tam = sizeof(struct sockaddr_in);
    current_user->sala = NULL;

		clilen = sizeof(struct sockaddr_in);
		if ((current_user->socket = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen)) == -1)
			puts("ERROR on accept");

		//cliente conecta: preenche a estrutura do cliente em
		// exclusao mutua pra nao dar conflito entre os users se conectando
		pthread_mutex_lock(&mutex_cliente);

		//adiciona o cliente na lista
		current_user->prox = users;
		users = current_user;

		bzero(buffer,256);

		n = read(current_user->socket, buffer, 256);
		if (n < 0)
			puts("ERROR reading from socket");

		//primeira msg eh o nome
		current_user->apelido = malloc(strlen(buffer+1));
		strcpy(current_user->apelido, buffer);

		entra_sala("Sala Geral", current_user);

		//boas vindas ao cliente
		bzero(buffer, 256);
		sprintf(buffer, "BEM VINDO! VOCE ESTA NA SALA GERAL\n");
		int n = write(current_user->socket, buffer, sizeof(buffer));
		if (n < 0)
		{
			puts("There was an Error writing to socket.");
		}
    //ajuda ao cliente
    ajuda(current_user);

		//cria a thread do usuario conectado
		pthread_create(current_user->thr, NULL, client, (void*) current_user);

		pthread_mutex_unlock(&mutex_cliente);
	}

	return 0;
}
