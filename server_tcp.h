
/*
    Para desenvolver as salas, foi utilizada uma estratégia de lista de salas.
*/

typedef struct sala
{
    char *nome;
    int qte_usuarios;
    int max_usuarios;
    int id;
    struct sala *prox;
    struct cliente **clientes;

}sala;

typedef struct cliente
{
    char *apelido;
    int socket; //identificador do socket do usuário
    struct sockaddr_in *addr; //informação para conexão com o cliente
    int socket_tam;
    pthread_t *thr;
    struct sala *sala; //sala que o cliente ta
    struct cliente *prox;
}cliente;

sala *add_nova_sala(char* novo_nome, int novo_max_usuarios);
void entra_sala(char *nome, cliente *user);
sala *acha_sala(char *nome);
void sai_sala(cliente *user);
void remove_sala(sala *sala);
void manda_msg_grupo(cliente *user, char *msg);
void manda_msg_todos(char *msg);
void lista_participantes(sala *sala, cliente *user);
int comeca_com(const char *prefixo, const char *str);
void sair(cliente *user);
