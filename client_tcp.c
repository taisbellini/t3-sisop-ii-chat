/*
Como compilar:
gcc client_tcp.c -o client -lpthread

Como usar:
./client 127.0.0.1
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "server_tcp.h"


#define PORT 4000

int comeca_com(const char *prefixo, const char *str)
{
	int comeca;

	comeca = strncmp(prefixo, str, strlen(prefixo));
	return comeca == 0;
}

//implementação com exclusão mútua
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//representa a thread do cliente
pthread_t thr;

char buffer[256];

int sockfd, n;
struct sockaddr_in serv_addr;
struct hostent *server;

void *servidor(void *arg)
{
	char buffer[256];
	int n;

	while(1)
	{
		bzero(buffer,256);
		n = read(sockfd, buffer, 256);
		pthread_mutex_lock(&mutex);
		if (n < 0) {
			printf("ERROR reading from socket\n");
		    exit(-1);
		}
		printf("%s\n", buffer);
		pthread_mutex_unlock(&mutex);
	}
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
		fprintf(stderr,"usage %s hostname\n", argv[0]);
		exit(0);
    }

	server = gethostbyname(argv[1]);
	if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
      printf("ERROR opening socket\n");

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	serv_addr.sin_addr = *((struct in_addr *)server->h_addr);
	bzero(&(serv_addr.sin_zero), 8);

	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
        printf("ERROR connecting\n");
        exit(-1);
    }

  //Criação do apelido
	bzero(buffer, 256);
  	printf("Seu Apelido: ");
  	fgets(buffer, 256, stdin);

		char *fim;

		// remove o \n do fim do apelido
		if((fim = strchr(buffer,'\n')) != NULL ){
			*fim = '\0';
		}

  //escreve no socket
	n = write(sockfd, buffer, strlen(buffer));
	if (n < 0)
		printf("ERROR writing to socket\n");

	pthread_create(&thr, NULL, servidor, NULL);

	while(1){


		bzero(buffer, 256);
		__fpurge(stdin);
		fgets(buffer, 256, stdin);

		//faz a leitura em exclusao mutua pra nao dar confusao
		pthread_mutex_lock(&mutex);

		char *fim;

		// remove o \n do fim do apelido
		if((fim = strchr(buffer,'\n')) != NULL ){
			*fim = '\0';
		}

		//write in the socket
		n = write(sockfd, buffer, strlen(buffer));
		if (n < 0)
			printf("ERROR writing to socket\n");

		if(comeca_com("/exit", buffer)){
			close(sockfd);
			exit(0);
		}

		pthread_mutex_unlock(&mutex);
	}

	close(sockfd);
  return 0;
}
