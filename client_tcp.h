

/*
    Para desenvolver as salas, foi utilizada uma estratégia de lista de salas.
*/

typedef struct sala
{
    char *nome;
    int qte_usuarios;
    int max_usuarios;
    struct sala *prox;
    struct cliente **clientes;

}sala;

typedef struct cliente
{
    char *apelido;
    int socket; //identificador do socket do usuário
    struct sockaddr_in *addr; //informação para conexão com o cliente
    int socket_tam; //tamanho do socket (tais - ?)
    pthread_t *thr; //thread do cliente
    struct sala *salas; //lista de salas do cliente
    struct clientes *prox;
}cliente;
